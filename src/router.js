import Game from '@/components/Game.vue'
import Menu from '@/components/Menu.vue'
import Router from 'vue-router'

export default new Router({
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'menu',
            component: Menu,
        },
        {
            path: '/game/:gameId',
            name: 'game',
            component: Game,
            props: route => ({ gameId: route.params.gameId }),
        },
    ],
})
