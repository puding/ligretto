import Vue from 'vue'
import App from './App.vue'
import { db } from '@/db.js'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy)

import { firestorePlugin } from 'vuefire'
Vue.use(firestorePlugin)

import VueRouter from 'vue-router'
import router from '@/router'
Vue.use(VueRouter)

Vue.config.productionTip = false

new Vue({
    router,
    data() {
        return {
            isLoading: true,
            players: [],
            games: [],
            members: {},
        }
    },
    computed: {
        playersById() {
            let result = {}
            for (const player of this.players) {
                result[player.id] = player
            }
            return result
        },
    },
    methods: {
        loadMembers() {
            let gamePromises = []
            for (let game of this.games) {
                this.members[game.id] = []
                const gamePromise = this.$bind(
                    `members.${game.id}`,
                    db
                        .collection('games')
                        .doc(game.id)
                        .collection('members')
                )
                gamePromises.push(gamePromise)
            }
            return Promise.all(gamePromises)
        },
    },
    mounted() {
        Promise.all([
            this.$bind('players', db.collection('players').orderBy('name')),
            this.$bind('games', db.collection('games').orderBy('created', 'desc')),
        ])
            .then(this.loadMembers)
            .then(() => (this.isLoading = false))
    },
    render: h => h(App),
}).$mount('#app')
