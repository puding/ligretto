import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/analytics'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: 'AIzaSyB1i9rdBC4ps6RhBrLP8sYahGYeN18_OWY',
    authDomain: 'ligretto-30512.firebaseapp.com',
    projectId: 'ligretto-30512',
    storageBucket: 'ligretto-30512.appspot.com',
    messagingSenderId: '375355162094',
    appId: '1:375355162094:web:90a07b0774e62918ab22ff',
    measurementId: 'G-9TLNESJVZG',
}

export const db = firebase.initializeApp(firebaseConfig).firestore()
firebase.analytics()

export const Timestamp = firebase.firestore

export const auth = firebase.auth
