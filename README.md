# Ligretto Scoreboard

Simple Scoreboard for [Ligretto](https://en.wikipedia.org/wiki/Ligretto) written in Vue with
Firebase as the backend (you can read more about the Firebase setup in [firebase.md](firebase.md)).

Please note that only whitelisted authenticated users are allowed to add new players, games and
rounds.

![](images/game.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
